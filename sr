#!/usr/bin/env fennel

(fn load-questions [questions-path]
  (collect [line (io.lines questions-path)]
    (let [(question definition) (string.match line "(.*)\t(.*)")]
      (values question definition))))

(fn load-guesses [guesses-path]
  (let [load! (fn []
                (icollect [line (io.lines guesses-path)]
                  (let [(time question grade) (string.match line "(.*)\t(.*)\t(.*)")]
                    {:time (tonumber time)
                     :question question
                     :grade (tonumber grade)})))]
    (case (pcall load!)
      (true guesses) guesses
      (false _) []))) ; Likely just a missing file

; Ported from https://en.wikipedia.org/wiki/SuperMemo
(fn supermemo2 [grade {: interval : repetitions : easiness}]
  {:interval (if (< grade 3)
               1
               (case repetitions
                 0 1
                 1 6
                 _ (* interval easiness)))
   :repetitions (if (< grade 3)
                  1
                  (+ repetitions 1))
   :easiness (math.max 1.3
                       (+ easiness (- 0.1 (* (- 5 grade)
                                             (+ 0.08 (* 0.02 (- 5 grade)))))))})

(fn sort-by-review-order [questions guesses]
  (let [initial-stats (collect [question (pairs questions)]
                        (values question {:repetitions 0 :easiness 2.5 :interval 0}))
        stats (accumulate [stats initial-stats
                           _ {: time : question : grade} (ipairs guesses)]
                (let [question-stats (supermemo2 grade (. stats question))]
                  (tset question-stats :last-reviewed time)
                  (tset stats question question-stats)
                  stats))
        to-sort (icollect [question {: last-reviewed : interval} (pairs stats)]
                  {: question
                   :definition (. questions question)
                   :review-time (+ (or last-reviewed 0)
                                   (* interval 86400))})
        by-review-time #(< $1.review-time $2.review-time)]
      (doto to-sort
       (table.sort by-review-time))))

(fn empty-string? [s]
  (or (= nil s)
      (= "" (string.gsub s "%s" ""))))

(fn record-guess! [guesses-path question grade]
  (with-open [file (io.open guesses-path :a)]
    (file:write (os.time) "\t" question "\t" grade "\n")))

(fn play [guesses-path questions-due]
  (case questions-due
    [{: question : definition} & remaining-questions-due]
    (do (io.write "\n" question "\n> ")
        (let [(input? input) (pcall #(io.read :*l))
              correct? (and (not (empty-string? input))
                            (string.match definition input))
              incorrect? (and (not (empty-string? input))
                              (not correct?))]
          (when input?
            (print (string.format "%s\"%s\""
                                  (if correct? ":) "
                                      incorrect? ":( "
                                      :else "")
                                  definition))
            (record-guess! guesses-path question (if correct? 4 1)) ; TODO: More granular grades
            (play guesses-path remaining-questions-due))))))

(fn take [n tbl]
  (local new-tbl [])
  (each [_ e (ipairs tbl) &until (= n (length new-tbl))]
    (table.insert new-tbl e))
  new-tbl)

(fn usage! []
  (print "usage: sr [-n=10] [--questions-path=~/.sr/questions/tok.tsv] [--guesses-path=~/.sr/guesses/tok.tsv] [shorthand]")
  (os.exit 0))

(fn parse-options [args]
  (let [default-questions-dir (.. (os.getenv "HOME") "/.sr/questions/")
        default-guesses-dir (.. (os.getenv "HOME") "/.sr/guesses/")]
    (local options {:max-questions 10
                    :questions-path (.. default-questions-dir "tok.tsv")
                    :guesses-path (.. default-guesses-dir "tok.tsv")})
    (each [_ arg (ipairs args)]
      (let [flag-arg? (string.match arg "^%-")
            [option value] (if flag-arg?
                             [(string.match arg "%-+([^=]+)") (string.match arg "=(%S+)")]
                             [:shorthand arg])]
        (case option
          :help (usage!)
          :questions-path (tset options :questions-path value)
          :guesses-path (tset options :guesses-path value)
          :n (tset options :max-questions (tonumber value))
          :shorthand (do (tset options :questions-path (.. default-questions-dir value ".tsv"))
                         (tset options :guesses-path (.. default-guesses-dir value ".tsv")))
          _ (error (.. "unknown option: " (or option "<nil>"))))))
    options))

(fn init []
  (let [{: max-questions : questions-path : guesses-path} (parse-options arg)
        all-questions (load-questions questions-path)
        prior-guesses (load-guesses guesses-path)
        questions-in-review-order (sort-by-review-order all-questions prior-guesses)
        questions-to-review (take max-questions questions-in-review-order)]
    (play guesses-path questions-to-review)))

(init)
