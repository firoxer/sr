sr
===

Text-based [spaced-repetition](https://en.wikipedia.org/wiki/Spaced_repetition) tool, written in [Fennel](https://fennel-lang.org/).

`sr` is useful as an aid for learning a foreign language or advanced terminology. The tool shows you a question and prompts you for a guess. Your guess is then matched against the answer via simple substring comparison. The more you do this, the more relevant questions you get and the more you memorise.

The software comes with a word list for Toki Pona to demonstrate usage.

Installation
---

Copy the `sr` executable:

```
sudo cp sr /usr/local/bin/sr 
```

Then copy the question and guess directories:

```
cp .sr ~/.sr
```

Usage
---

```
$ sr [-n=10] [--questions-path=questions/tok.tsv] [--guesses-path=guesses/tok.tsv] [shorthand]
```

A typical session consists of a series of questions, prompts and answers with smiley feedback, like this:

```
$ sr -n=3

lon
> located at
:) "located at, present at, real, true, existing"

nasin
>
"way, custom, doctrine, method, path, road"

sewi
> animal
:( "area above, highest part, something elevated; awe-inspiring, divine, sacred, supernatural"

*exit*
```

As a convention, if you have questions in the `~/.sr/questions` directory, you can use them with the shorthand `sr [file]`. For example, running `sr ukr` would ask questions from `~/.sr/questions/ukr.tsv` and record your guesses to `~/.sr/guesses/ukr.tsv`.

### Adding Questions

Create a TSV file under `~/.sr/questions`. For example, if you wanted to memorise bauen, "to build" in German, you could run `echo 'bauen\tto build' >>$HOME/.sr/questions/deu.tsv`.

You could also create a shell function shortcut for this, e.g. `function sr-add() { echo "$2" >>$HOME/.sr/questions/$1.tsv }`. Then, to add questions, you would run `sr-add deu 'bauen\tto build'`.

A Frequently Asked Question
---

> Why did you pick Toki Pona as the demo question set?

Because that's what I originally used the software for, and because it's a nice, closed set.

Credits
---

The Toki Pona list of words is derived from the [linku.la](https://linku.la/about/jasima/) API.
